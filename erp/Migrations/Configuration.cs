namespace Erp.DataAccess.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Erp.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Erp.DataAccess.ErpContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Erp.DataAccess.ErpContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

          
            context.Suppliers.AddOrUpdate(
                new Supplier
                {
                    Name = "Tesla"
                },
                new Supplier
                {
                    Name = "Ford"
                },
                new Supplier
                {
                    Name = "General Motors"
                });

            context.SaveChanges();

            context.Products.AddOrUpdate(
                    new Product { Name = "Model X", Rate = 20000, SupplierId = 1 },
                    new Product { Name = "Mustang", Rate = 20000, SupplierId = 2 },
                    new Product { Name = "Figo", Rate = 20000, SupplierId = 2 },
                    new Product { Name = "Aspire", Rate = 20000, SupplierId = 2 },
                    new Product { Name = "Fiesta", Rate = 20000, SupplierId = 2 },
                    new Product { Name = "Chevrolet", Rate = 20000, SupplierId = 3 },
                    new Product { Name = "Buick", Rate = 20000, SupplierId = 3 },
                    new Product { Name = "Caddilac", Rate = 20000, SupplierId = 3 }
                );

            context.SaveChanges();
            
        }
    }
}
