﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Models
{
    public class Grn
    {
        private readonly ObservableListSource<GrnProduct> _grnProducts =
            new ObservableListSource<GrnProduct>();

        public int Id { get; set; }
        public int SupplierId { get; set; }
        public string Note { get; set; }
        public double Total { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual ObservableListSource<GrnProduct> GrnProducts { get { return _grnProducts; } }
    }
}
