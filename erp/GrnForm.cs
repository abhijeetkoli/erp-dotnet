﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using Erp.Models;
using Erp.DataAccess;

namespace erp
{
    public partial class GrnForm : Form
    {
        ErpContext _context;
        List<Supplier> _suppliers;
        List<Product> _products;

        DataGridViewComboBoxColumn _productsCombo;

        public GrnForm()
        {
            InitializeComponent();
        }

        private void GrnForm_Load(object sender, EventArgs e)
        {

            _context = new ErpContext();

            _suppliers = _context.Suppliers.OrderBy(s => s.Name).ToList();
            _products = _context.Products.OrderBy(p => p.Name).ToList();

            supplierBindingSource.DataSource = _suppliers;
            productBindingSource.DataSource = _products;

            _context.Grns.Load();

            grnBindingSource.DataSource = _context.Grns.Local.ToBindingList();

            FillCombos();

        }

        private void FillCombos()
        {
            //grn supplier
            supplierCombo.DisplayMember = "Name";
            supplierCombo.ValueMember = "Id";
            supplierCombo.DataSource = _suppliers;
            supplierCombo.DataBindings.Add(new Binding("SelectedItem", grnBindingSource, "Supplier"));

            //grn products product combo            
            _productsCombo = (DataGridViewComboBoxColumn)productsGrid.Columns["productId"];
            _productsCombo.DataSource = _products;
            _productsCombo.ValueType = typeof(int);
            _productsCombo.DataPropertyName = "ProductId";
            _productsCombo.DisplayMember = "Name";
            _productsCombo.ValueMember = "Id";           
        }

        private void NewGrn()
        {
            grnBindingSource.AddNew();
            supplierCombo.Focus();
        }

        private void SaveGrn()
        {
            grnBindingSource.EndEdit();

            try
            {
                _context.SaveChanges();
            } catch(Exception e)
            {
                MessageBox.Show(e.InnerException.ToString());
            }
            
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            NewGrn();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveGrn();
        }


        private void grnBindingNavigator_RefreshItems(object sender, EventArgs e)
        {

        }

        private void grnProductSaveButton_Click(object sender, EventArgs e)
        {
            SaveGrn();
        }

        private void productsGrid_DataError_1(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Context == (DataGridViewDataErrorContexts.Formatting | DataGridViewDataErrorContexts.PreferredSize))
            {
                e.ThrowException = false;
            }
        }

        private void productsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void productsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //rate or quantity changed
            if (new int[]{3, 4}.Contains(e.ColumnIndex) && e.RowIndex >= 0)
            {
                var selecteProductId = productsGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                UpdateRowAmount(productsGrid.CurrentRow);
            }
        }

        private void productsGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int columnIndex = productsGrid.CurrentCell.ColumnIndex;

            //product combobox event handlers
            if(columnIndex == _productsCombo.Index)
            {
                ComboBox productsCombo = (ComboBox)e.Control;

                productsCombo.SelectedIndexChanged -= new
                EventHandler(ProductComboBox_SelectedIndexChanged);
                
                productsCombo.SelectedIndexChanged += new
                EventHandler(ProductComboBox_SelectedIndexChanged);
            }
        }

        private void ProductComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            Product selectedProduct = (Product)((ComboBox)sender).SelectedItem;

            if(selectedProduct != null)
                productsGrid.CurrentRow.Cells["rate"].Value = selectedProduct.Rate;         
        }

        private void UpdateRowAmount(DataGridViewRow row)
        {
            double Rate = (double)row.Cells["rate"].Value;
            int Quantity = (int)row.Cells["quantity"].Value;

            row.Cells["amount"].Value = Rate * Quantity;

            UpdateGrnTotal();
        }

        private void UpdateGrnTotal()
        {
            double amount = 0.00;

            foreach(DataGridViewRow row in productsGrid.Rows)
            {
                if(row.Cells["amount"].Value != null)
                    amount += (double)row.Cells["amount"].Value;
            }

            totalText.Text = amount.ToString();
        }

    }
}
