﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Models
{
    public class Grn
    {
        public int Id { get; set; }
        public int SupplierId { get; set; }
        public string Note { get; set; }
        public double Total { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual List<GrnProduct> GrnProducts { get; set; }
    }
}
