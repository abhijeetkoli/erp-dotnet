﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Models
{
    public class GrnProduct
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int GrnId { get; set; }
        public double Rate { get; set; }
        public int Quantity { get; set; }
        public double Amount { get; set; }

        public virtual Product Product { get; set; }
        public virtual Grn Grn { get; set; }
    }
}
