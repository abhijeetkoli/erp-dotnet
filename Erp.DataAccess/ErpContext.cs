namespace Erp.DataAccess
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Erp.Models;

    public class ErpContext : DbContext
    {
        // Your context has been configured to use a 'ErpContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Erp.DataAccess.ErpContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ErpContext' 
        // connection string in the application configuration file.
        public ErpContext()
            : base("name=ErpContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Grn> Grns { get; set; }
        public virtual DbSet<GrnProduct> GrnProducts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Supplier>()
                .HasMany(s => s.Products)
                .WithRequired()
                .HasForeignKey(p => p.SupplierId)
                .WillCascadeOnDelete(false);
        }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}